# Sicherungsskript usr-bak

Das Skript sichert das Benutzerverzeichnis auf ein Serververzeichnis.

Dazu werden ssh und rsync verwendet.

Weitere Erklärungen befinden sich am Anfang des Skripts.

Erklärungen zum Ausschluss von Dateien von der Sicherung befinden sich am Anfang der Datei user-exclude.txt

---

The program and the files are protected by copyright. 
Copyright (c) 2022
SPDX-License-Identifier: GPL-3.0-or-later
<https://www.gnu.org/>
