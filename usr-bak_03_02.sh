#!/bin/bash
#
# Script zu Sicherung des eigenen Benutzerverzeichnis auf einen Server mit rsync über SSH.
#
# VERBINDUNG ÜBER SSH
# Für die Ausführung des Programms und der darin enthaltenen rsync-Befehle muss eine Verbindung über
# SSH mit dem Server möglich sein.
# Dafür kann z.B. ein Kerberos-Ticket oder Keyfiles verwendet werden.
# Pro Sicherungs- oder Verleichsvorgang erfolgen zwei Verbindungen über SSH, für die auch jeweils ein Passwort eingegeben
# werden kann.
#
# SICHERUNGSVERZEICHNIS
# Die differenzielle Sicherung erfolgt in das Verzeichnis "server:/mnt/data/backup/$USER/$HOSTNAME".
# Die Unterverzeichnisse "/mnt/data/backup/$USER" müssen vorher mit Schreibrechten für den Benutzer auf dem Server angelegt sein.
# Die Hostnamen auf den zu sichernden Geräten müssen statisch sein, weil in den Verzeichnispfaden die Umgebungsvariabelen benutzt werden.
#
# LOGDATEIEN
# Für die Log-Dateien muss das Verzeichnis "/home/$USER/log/" auf dem Host vorhanden sein,
# sonst wird es vom Programm angelegt.
#
# SPEICHERORT DER PROGRAMMDATEI
# Die Programmdatei sollte unter '/usr/local/bin' mit den Rechten '-rwxr-xr-x 1 root root' gespeichert werden,
# damit es für alle Benutzer verwendbar ist.
#
# EXCLUDE FILES
# Es können bis zu drei Exclude-Dateien verwendet werden:
#   1.) Eine nicht anpassbare Excludedatei, die im Programmverzeichnis gespeichert sein sollte.
#   2.) Eine für jeden Benutzer anpassbare Excludedatei, die im Verzeichnis '/home/$USER/bin/user.exclude' gespeichert sein sollte.
#   3.) Eine für jeden Benutzer beliebig speicherbare Excluddatei, die als Parameter beim Programmstart angegeben wird.
# Die tatsächlichen Verzeichnisse sind weiter unten unter 'Variablen/Exclude Files' eingetragen.
#
# AUSFÜHRUNGSRECHTE
# Das Skript muss nur dann als sudo ausgeführt werden, wenn andere als die eigenen Benutzerrechte bei der Sicherung
# erhalten bleiben sollen!
#
# SONSTIGE HINWEISE:
# Das Backupverzeichnis kann in die /etc/fstab eingebunden werden:
# marvin:/backup/henk	/home/henk/mnt/backup	nfs     sec=krb5i,vers=4.2,noauto,user  0  0
#
# ZUR ENTWICKLUNG:
#
# Die Ausgabe von Umgebungsvariablen am Prompt kann zur Prüfung erfolgen. Beispiel: echo $HOSTNAME
#
# Anmerkung zu der Sortierung nach Zeit mit ls:
# Bei der Sortierung nach Zeit (-t) wird die Zeit von mtime benutzt. Das hat sich als unzuverlässig herausgestellt
# Deswegen wird nach ctime sortiert (-tc)
# Die für eine Datei oder Verzeichnis gespeicherten Zeitstempel können mit stat <dateiname> ausgegeben werden.
# Estamps normally recorded:
#    mtime — updated when the file contents change. This is the "default" file time in most cases.
#    ctime — updated when the file or its metadata (owner, permissions) change
#    atime — updated when the file is read
#
# ls Optionen
#  -t     Nach Änderungszeit sortieren, neueste zuerst; siehe --time
# --time=WORT   ändert die Vorgabe für die Verwendung von Änderungszeiten; 
#                   Zugriffszeit (-u): atime, access, use; 
#                   Änderungszeit (-c): ctime, status;
#                   Erstellungszeit: birth, creation; (nur suse)
# -c    with -lt: sort by, and show, ctime (time of last modification of file status information);
# -r, --reverse     umgekehrte Reihenfolge beim Sortieren
#
# Verschachtelte Vergleiche:
# https://stackoverflow.com/questions/3826425/how-to-represent-multiple-conditions-in-a-shell-if-statement
#
#
# Copyright (c) 2022
# SPDX-License-Identifier: GPL-3.0-or-later
#https://www.gnu.org/

###############################################################################################
# Variablen
###############################################################################################

### Programmversion
prog_ver=03.02

###
### Die folgenden Variablen können durch den Benutzer angepasst werden
###
### Anzahl der differenziellen Sicherungen, die beim Ereichen der Speichergrenze NICHT gelöscht werden (Voreinstellung: 21)
min_ver=21

### maximaler Speicher in byte (Speichergrenze, Voreinstellung: 50 GB / 50000000 B) 
max_mem=50000000
### Anzahl der maximale möglichen Sicherungsversionen (Voreinstellung: 100)
max_ver=100

### Benutzerrückfrage vor dem Löschen alter Versionen beim Erreichen der Speichergrenze oder maximal möglichen Versionen AUSSTELLEN (Voreinstellung: yes)
auto_remove=yes

###
### Die weiteren Variablen sollten nicht durch den Benutzer angepasst werden
###

# Zeitstempel zur Kennzeichnung der Sicherungsverzeichnisse
timeStamp=$(date +%Y-%m-%d_%H-%M-%S)

### Benutzername (muss mit dem Namen auf dem Server übereinstimmen)
host_user=$USER

### Computernamen
server_name=marvin
#server_name=brikett
#server_name=192.168.8.106
host_name=$HOSTNAME

### Verzeichnisse und Dateien

# Logdaten
log_dir=/home/${USER}/log
diff_back_log=${log_dir}/${USER}_diff_backup.log
diff_comp_log=${log_dir}/${USER}_comp_backup.log

# Zu sicherndes Verzeichnis
backup_source_dir=/home/${USER}/
#backup_source_dir=/home/${USER}/test/

# Zielverzeichnis der Sicherung auf dem Server
backup_target_dir=/mnt/data/backup/${USER}/${HOSTNAME}
#backup_target_dir=/mnt/data/backup/${USER}/test

## Name des symbolischen Links zur letzten Sicherung
sym_link=00_letzte_sicherung


### Exclude-Files
MY_PATH=`dirname "$0"`
MY_PATH=`( cd ${MY_PATH} && pwd )`
#echo "${MY_PATH}"
exclude_from=${MY_PATH}/usr-bak-exclude_02_00.txt
user_exclude_from=/home/${USER}/bin/user-exclude.txt
# Alte Dateinamen:
#exclude_from=/usr/local/bin/backup-02.exclude
#user_exclude_from=/home/${USER}/bin/user-exclude
#
# Es besteht auch die Möglichkeit, eine Excludedatei beim Programmaufruf als Parameter anzugeben.
# In der Variablen "exclude" werden später alle angegebnen Excludedateien zusammengefasst.
exclude=''

### Weitere verwendete Variablen
# Zum Auswerten des Rückgabewerts von Funktionen:
status=""
# Zur Menüauswahl:
auswahl=""
# Ermittelter benutzter Speicher:
# storageSpace

###############################################################################################
# Funktionen
###############################################################################################

########## Funktion: Verzeichnis auf dem Server zum Sichern vorbereiten / Versionsnummer ermitteln
diff_back() {
    min_ver=$1
    host_user=$2
    host_name=$3
    backup_target_dir=$4
    sym_link=$5
    max_mem=$6
    auto_remove=$7
    max_ver=$8

        # Prüfe, ob das Unterverzeichnis mit dem Computernamen vorhanden ist, falls nicht, lege es an
		if [[ ! -d ${backup_target_dir} ]] ; then
			echo "Noch kein Sicherungsverzeichnis ${backup_target_dir} vorhanden."
			echo "Lege Sicherungsverzeichnis ${backup_target_dir} an."
			mkdir ${backup_target_dir}
		fi
		# Prüfe, ob ein alter symbolischer Link vorhanden ist und lösche ihn
		if [ -L ${backup_target_dir}/${sym_link} ]; then
            echo "Symlink gefunden. Entferne alten Symlink."
            rm ${backup_target_dir}/${sym_link}
        fi
		
		# Ermittle das letzte Sicherungsverzeichnis und die Anzahl der vorhandenen Sicherungsverzeichnisse
		# Neustes Sicherungsverzeichnis ermitteln (Alternativ)
		# -tc sortiern nach change time; -d Verzeichnis‐Einträge statt der Inhalte anzeigen, symbolische Verknüpfungen nicht verfolgen
        neustes_verzeichnis=$(ls -tcd ${backup_target_dir}/*/ | head -n 1)
        echo "Neustes vorhandenes Backupverzeichnis: ${neustes_verzeichnis}"
        num_ver=$(ls -d ${backup_target_dir}/*/ | wc -l)
        echo "Anzahl der Verzeichnisse: ${num_ver}"
        echo "Größte erlaubte Anzahl: ${max_ver}"
        echo "Mindestens zu erhaltende Verzeichnisanzahl: ${min_ver}"
        echo        
        # Benutzten Speicherplatz für die Sicherung dieses Hosts ermitteln
        # du -s and cut -f1 are specified by POSIX and are therefore portable to any Unix system.
        echo "Der durch die Sicherungen bereits belegte Speicher wird ermittelt."
        storageSpace=$(du -s ${backup_target_dir} | cut -f1)
        #printf '%s\n' "Verzeichnisse: $num"
        echo "Benutzer Speicher: ${storageSpace}"
        echo "Speichergrenze:    ${max_mem}"
        echo
        
        # Speichergrenze oder maximale Verzeichnisanzahl / Versionsanzahl erreicht? Dann löschen.
        while ([[ ${num_ver} -gt ${min_ver} ]] && [[ ${storageSpace} -gt ${max_mem} ]]) || ([[ ${num_ver} -gt ${min_ver} ]] && [[ ${num_ver} -gt ${max_ver} ]])
        do
            echo "Die Speichergrenze erreicht."
            oldest_backup_dir=$(ls -tcrd ${backup_target_dir}/*/ | head -n 1)
            if [[ ! ${auto_remove} = "yes" ]]; then
                a=""
                echo "Soll das älteste Backupverzeichnis gelöscht werden? (y/N):  ${oldest_backup_dir}"
                read a
                if [[ ! ${a} = "y" ]] || [[ ! ${a} = "y" ]]; then
                    echo "Dann halt nicht ..."
                    echo
                    break
                fi
            fi
            echo "Lösche das älteste Backupverzeichnis:  ${oldest_backup_dir}"
            rm -rf ${oldest_backup_dir}
            ((num--))            
            echo "Der durch die Sicherungen bereits belegte Speicher wird ermittelt."
            storageSpace=$(du -s ${backup_target_dir} | cut -f1)
            echo "Benutzer Speicher nach dem Löschen: ${storageSpace}"
            echo "Speichergrenze:    ${max_mem}"
            echo "Anzahl der verbleibenden Verzeichnisse nach dem Löschen: ${num_ver}"
            echo "Größte erlaubte Anzahl: ${max_ver}"
            echo "Mindestens zu erhaltende Verzeichnisanzahl: ${min_ver}"
            echo
        done
		
		# Erzeuge einen Symbolischen Link auf das letzte gfundene (Sicherungs-) Verzeichnis ${neustes_verzeichnis} statt ${name}
        if [ -d "${neustes_verzeichnis}" ]; then
            #echo "(Neuste) Sicherung gefunden: ${neustes_verzeichnis}"
            echo "Lege Symlink zum neusten Verzeichnis an."
            ln -s ${neustes_verzeichnis} ${backup_target_dir}/${sym_link}
            else
            echo "Noch keine Sicherung vorhanden."
            exit 255
        fi
		return 0
}

########## Funktion: Letztes Sicherungsverzeichnis zum Vergleichen auf dem Server ermitteln
diff_comp() {
    min_ver=$1
    host_user=$2
    host_name=$3
    backup_target_dir=$4
    sym_link=$5
    # Prüfe, ob ein Sicherungsverzeichnis vorhanden ist
    if [[ ! -d ${backup_target_dir} ]] || [[ $(ls -d ${backup_target_dir}/${host_user}_*/ 2>/dev/null | wc -l) < 1  ]] ; then
        echo "suchte nach: ${backup_target_dir}/${host_user}*/"
        echo "Kein Backupverzeichnis vorhanden."
        return 255
    fi
    # Prüfe, ob ein alter symbolischer Link vorhanden ist und lösche ihn
    if [ -L ${backup_target_dir}/${sym_link} ]; then
        echo "Symlink gefunden. Entferne alten Symlink."
        rm ${backup_target_dir}/${sym_link}
    fi
    # Ermittle das neuste Sicherungsverzeichnis und die Anzahl der vorhandenen Sicherungsverzeichnisse
    neustes_verzeichnis=$(ls -tcd ${backup_target_dir}/*/ | head -n 1)
    #echo "Neustes Backupverzeichnis: ${neustes_verzeichnis}"
    
    # Erzeuge einen Symbolischen Link auf das letzte gfundene (Sicherungs-) Verzeichnis ${neustes_verzeichnis} statt ${name}
    if [ -d "${neustes_verzeichnis}" ]; then
        echo "Neustes vorhandenes Backupverzeichnis: ${neustes_verzeichnis}"
        echo "Lege neuen Symlink zum neusten Verzeichnis an."
        ln -s ${neustes_verzeichnis} ${backup_target_dir}/${sym_link}
        else
        echo "Noch keine Sicherung vorhanden."
        exit 255
    fi
    return 0
}


###############################################################################################
# Hauptprogramm
###############################################################################################

### Benutzerhinweise
echo
echo "####################"
echo "Backup auf ${server_name}"
echo "Programmversion ${prog_ver}"
echo "####################"
echo
echo "Hallo ${USER},"
echo
echo "mit diesem Script werden die Daten aus deinem Benutzerverzeichnis"
echo "/home/${USER} gesichert."
echo
echo "Die Sicherung erfolgt in ein Verzeichnis, das in"
echo "${server_name}:${backup_target_dir} angelegt wird."
echo
echo "Logfiles werden unter ${log_dir} angelegt."
echo
echo "Der Datenabgleich (\"Daten Prüfen\") erfolgt mit dem"
echo "neusten Verzeichnis der differentiellen Sicherung."
echo 
echo "Von der Sicherung ausgenommenen sind Dateien und Verzeichnisse,"
echo "die in einer der Exclude-Dateien angegeben sind."
echo "Exclude-Dateien werden zusammengefasst:"
echo

### Prüfen der Excludedateien
echo -n '1.) '
if [ -f ${exclude_from} ] ; then
    echo "Füge programmeigenen Excludefile an: ${exclude_from}"
    exclude="--exclude-from=${exclude_from}"
    else
    echo "Programmeigenen Excludefile nicht gefunden:${exclude_from}"
    exit 41
fi
echo -n '2.) '
if [ -f ${user_exclude_from} ] ; then
    echo "Füge vordefinierten Benutzer-Excludefile an: ${user_exclude_from}"
    exclude="$exclude --exclude-from=${user_exclude_from}"
    else
    echo "Vordefinierten Benutzer-Excludefile ${user_exclude_from} nicht gefunden"
fi
echo -n '3.) '
if [ -f ${1} ] && [ ${1} ] ; then
        echo "Füge beim Programmaufruf angegebenen Excludefile: ${1}"
        exclude="$exclude --exclude-from=${1}"
    elif [ ! -f ${1} ] ; then
        echo "Beim Programmaufruf angegebenen Excludefile \"${1}\" nicht gefunden."
        echo "Programm wird beendet."
        exit 42
    else
        echo "Es wurde keine zusätzlicher Excludefile beim Programmaufruf angegeben."
fi
echo
echo "Parameter für die Berücksichtigung von Exclude-Dateien:"
echo $exclude
echo

### Prüfen, ob das Log-Verzeichnis vorhanden ist, sonst anlegen
if [ ! -d ${log_dir} ] ; then
    echo "======================="
    echo
    echo "Verzeichnis für Logdateien nicht vorhanden."
    echo "Verzeichnis ${log_dir} wird angelegt."
    mkdir ${log_dir}
    echo
fi
echo "======================="
echo

### Auswahlmenü und Sicherungsausführung
echo "Menü:"
PS3="Wähle eine Nummer aus dem Menü: " # Ein neues Auswahl-Prompt für select

select auswahl in "Daten Prüfen" "Differenzielle Sicherung" Ende
do
        case "$auswahl" in
    ### Programmende / ungültige Eingabe
            Ende)   echo "Gewählt: Programmende"; break ;;
            "")     echo "Ungültige Auswahl" ;;
    ### Gesicherte Daten vergleichen
            "Daten Prüfen")
		    echo
		    echo "Das Homeverzeichnis soll mit der letzten Sicherung im Verzeichnis ${server_name}:${backup_target_dir} verglichen werden."
            echo "Der Vergleich wird vorbereitet"
            echo
        # Alten Logfile verschieben
        if [ -f ${diff_comp_log} ] ; then
            # echo Verschiebe Logfile ${diff_comp_log} nach ${diff_comp_log}.old
            mv ${diff_comp_log} ${diff_comp_log}.old
        fi
        # Vergleich auf dem Server vorbereiten
        ssh ${server_name} "$(declare -f diff_comp); diff_comp \"${min_ver}\" \"${USER}\" \"${HOSTNAME}\" \"${backup_target_dir}\" \"${sym_link}\""
        status=$?
        #echo "Rückgabewert der Funktion: ${status}"
        echo
        # Prüfe, ob das Unterverzeichnis mit dem Computernamen vorhanden ist, falls nicht, Hinweis und ENDE
        if [ ${status} = 255 ] ; then
			echo "Es ist noch kein Sicherungsverzeichnis ${backup_target_dir} vorhanden."
			echo "Es muss erst eine Sicherung durchgeführt werden, damit diese verglichen werden kann."
			break
		fi
		echo "Der Vergleich wird gestartet."
        echo "Es werden nur unterschiedliche Dateien und Verzeichnisse beim Vergleich ausgegeben:"
        rsync -axHI ${exclude} --dry-run --checksum --progress --log-file=${diff_comp_log} ${backup_source_dir} ${server_name}:${backup_target_dir}/${sym_link}
                break;;
    ### Sicherung durchführen
            "Differenzielle Sicherung")
        echo
        echo "Das Homeverzeichnis soll im Verzeichnis ${server_name}:${backup_target_dir} gesichert werden."
        echo "Die Sicherung wird vorbereitet"
        echo
        # Alten Logfile verschieben
        if [ -f ${diff_back_log} ] ; then
            mv ${diff_back_log} ${diff_back_log}.old
        fi
		# Sicherung auf dem Server vorbereiten
		ssh ${server_name} "$(declare -f diff_back); diff_back \"${min_ver}\" \"${USER}\" \"${host_name}\" \"${backup_target_dir}\" \"${sym_link}\" \"${max_mem}\" \"${auto_remove}\" \"${max_ver}\""
        status=$?
        #echo "Rückgabewert der Funktion: ${status}"
        # Wenn ${status} = 255 ist, dann wird eine erste Grundsicherung angelegt. (255 weil -1 nicht als Returnwert geht)
        # Sonst erfolgt eine differentielle Sicherung
        # Zeitstempel aktualisieren
        timeStamp=$(date +%Y-%m-%d_%H-%M-%S)
        if [ ${status} = 255 ] ; then
                echo "Es ist noch keine Sicherung vorhanden."
                echo "Lege erste Grundsicherung ${backup_target_dir}/${USER}_${timeStamp} an ..."
                rsync -axH ${exclude} --log-file=${diff_back_log} ${backup_source_dir} ${server_name}:${backup_target_dir}/${USER}_${timeStamp}
        else
            # Sicherungsvorgang
            echo "Differentielle Sicherung erfolgt in das Verzeichnis:  ${backup_target_dir}/${USER}_${timeStamp}"
            rsync -axH ${exclude} --delete --log-file=${diff_back_log} --link-dest=../${sym_link} ${backup_source_dir} ${server_name}:${backup_target_dir}/${USER}_${timeStamp}
        fi
                break;;
        esac
done
### Fenster bis zum drücken von ENTER geöffnet lassen
echo
echo "Das war´s."
echo "Logfiles unter /home/${USER}/log/"
echo "Fenster mit ENTER schließen ..."
read a
