#!/bin/bash

# Dateien zum Testen erzeugen:
# dd if=/dev/urandom of=schrott_$(date +%Y-%m-%d_%H-%M-%S) count=10 bs=10M

timeStamp=$(date +%Y-%m-%d_%H-%M-%S)
diff_target_dir=/mnt/Datenkeller/testbak/symtest
home_dir=/home/${USER}/test

if [ -L ${diff_target_dir}/0_sicherung ]; then
    echo "Symlink gefunden."
    rm ${diff_target_dir}/0_sicherung
fi

echo
echo "=========================================="
echo
i=0
#echo ${diff_target_dir}/*
for name in ${diff_target_dir}/*; do
#for name in ./symtest/*; do
    # if [ ! -f "$name" ] || [ -L "$name" ]; then
    if [ -L "$name" ]; then
        echo "nicht-reguläre Dateien und symbolische Links überspringen: ${name}"
        continue
    fi
    echo "i ist. ${i}"
    files+=( "$name" )
    echo "Inhalt von " '${files['${i}']} ist: ' ${files[$i]}
    #echo "files: $files"
    echo "name: $name"
    i=$(($i+1))
done

num=${#files[@]}
#  du -s and cut -f1 are specified by POSIX and are therefore portable to any Unix system.
storageSpace=$(du -s ${diff_target_dir} | cut -f1)
printf '%s\n' "Verzeichnisse: $num"
echo "Benutzer Speicher: ${storageSpace}"
echo "$name"

i=0
while [[ "${num}" -gt 8 ]] && [[ "${storageSpace}" -gt 300000 ]]
do
#if [[ "${storageSpace}" -gt 300000 ]]; then
#if [[ "${num}" -gt 3 ]]; then
    echo "Speichergrenze erreicht"
    echo "Lösche  ${files[$i]}"
    rm -rf ${files[$i]}
    storageSpace=$(du -s ${diff_target_dir} | cut -f1)
    echo "Benutzer Speicher nach löschen: ${storageSpace}"
    
    ((num=num-1))
    ((i++))
done

echo
echo "=========================================="
echo


#mkdir ${diff_target_dir}/${user}_${timeStamp}

# ln -s ZIEL LINK_NAME
if [ -d "${name}" ]; then
    echo "(Letzte) Sicherung gefunden: ${name}"
    #rm ${diff_target_dir}/0_sicherung
    ln -s ${name} ${diff_target_dir}/0_sicherung
    rsync -axH --delete --link-dest=../0_sicherung ${home_dir} ${diff_target_dir}/${USER}_${timeStamp}
    else
    echo "Noch keine Sicherung vorhanden."
    rsync -axH ${home_dir} ${diff_target_dir}/${USER}_${timeStamp}
fi

#rsync -axH --delete --link-dest=../sicherung ${home_dir} ${diff_target_dir}/${USER}_${timeStamp}
